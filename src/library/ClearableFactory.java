/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

/**
 *
 * @author BugInMyHEAD
 * @param <T> ICalculator
 */
public abstract class ClearableFactory<T extends Clearable> extends Factory<T>
{
	/**
	 * 
	 * @return A new instance of a {@code clear}ed user-defined clearable implementing {@code T}
	 */
	@Override
	public T produce()
	{
		T result = design();
		result.clear();

		return result;
	}

	/**
	 * 
	 * @return A new instance of a user-defined clearable implementing {@code T}
	 */
	@Override
	protected abstract T design();
}
