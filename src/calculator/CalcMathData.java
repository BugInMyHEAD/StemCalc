package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public abstract class CalcMathData extends CalcData implements ICalcMathData
{
	protected CalcMathData()
	{
	}
	
	@Override
	public CalcMathData getCalcData()
	{
		return this;
	}
}
