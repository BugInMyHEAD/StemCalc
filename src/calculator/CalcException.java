package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcException extends Exception
{
	public CalcException()
	{
		super();
	}
	
	public CalcException(String message)
	{
		super(message);
	}
}
