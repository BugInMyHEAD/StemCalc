package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcConstant extends CalcTextValue
{
	private final Double value;
	
	public CalcConstant(Double value)
	{
		this(value, null);
	}
	
	public CalcConstant(Double value, String text)
	{
		super(text);
		this.value = value;
	}

	@Override
	public Double getValue()
	{
		return value;
	}
}
