package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcWrongExpressionException extends CalcException
{
	public CalcWrongExpressionException()
	{
		super();
	}
	
	public CalcWrongExpressionException(String message)
	{
		super(message);
	}
}
