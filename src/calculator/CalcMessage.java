package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcMessage extends CalcData
{
	private final String text;
	
	public CalcMessage(String text)
	{
		this.text = text;
	}

	@Override
	public String toString()
	{
		return text;
	}
}
