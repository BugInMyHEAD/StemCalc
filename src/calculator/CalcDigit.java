package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcDigit extends CalcMathData
{
	/**
	 *
	 * @param text used for displaying a decimal point.
	 * @return {@code new CalcDigit(text, -1)}, which represents a decimal point.
	 * @see #CalcDigit(String, int)
	 */
	public static CalcDigit makePoint(String text)
	{
		return new CalcDigit(text, -1);
	}

	private final String text;

	/**
	 * {@code this} represents a decimal point if {@code value} is negative.
	 */
	public final int value;

	/**
	 * @param text displayed {@link String}
	 * @param value A digit's value.
	 */
	public CalcDigit(String text, int value)
	{
		this.text = text;
		this.value = value;
	}

	/**
	 *
	 * @return Whether {@code this} is a decimal point.
	 */
	public boolean isPoint()
	{
		return value < 0;
	}

	public boolean isZero()
	{
		return value == 0;
	}

	@Override
	public String toString()
	{
		return text;
	}
}
