package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public abstract class CalcValue extends CalcMathData
{
	protected CalcValue()
	{
	}

	public abstract Double getValue();
}
