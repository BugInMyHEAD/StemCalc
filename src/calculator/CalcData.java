package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public abstract class CalcData implements ICalcData
{
	protected CalcData()
	{
	}
	
	@Override
	public CalcData getCalcData()
	{
		return this;
	}
}
