package calculator;

import library.Nothing;
import java.util.Iterator;

/**
 *
 * @author BugInMyHEAD
 * @param <E>
 */
public interface ICalcDisplay<E extends ICalcMathData>
{
	ICalcDisplay<ICalcMathData> NO_DISPLAY = Nothing::doNothing;

	void update(Iterator<? extends E> it);
}
