package calculator;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;
import library.ClearableFactory;
import library.CountingIterator;
import library.CountingIteratorImpl;

/**
 *
 * @author BugInMyHEAD
 * @param <E>
 */
public abstract class Calculator<E extends ICalcMathData> implements ICalculator<E>
{
	private ICalcDisplay<? super E> display;
	private ICalcAlg alg;
	private CalcPriority priorities;
	private final Deque<Expression> inputDeque = new ArrayDeque<>();
	private final Deque<CalculatorInput> entry = new ArrayDeque<>();
	private boolean calcExceptionOccured;
	//final ArrayDeque<Integer> argCountDeque = new ArrayDeque<>(); // TODO
	
	class Expression implements Deque<CalculatorInput>
	{
		private final Deque<Deque<CalculatorInput>> deque = new ArrayDeque<>();

		@Override
		public void addFirst(CalculatorInput ci)
		{
			deque.addFirst(new WrapperForInput(ci));
		}

		@Override
		public void addLast(CalculatorInput ci)
		{
			deque.addLast(new WrapperForInput(ci));
		}

		@Override
		public boolean offerFirst(Deque<CalculatorInput> e)
		{
			return deque.offerFirst(e);
		}

		@Override
		public boolean offerLast(Deque<CalculatorInput> e)
		{
			return deque.offerLast(e);
		}

		@Override
		public Deque<CalculatorInput> removeFirst()
		{
			return deque.removeFirst();
		}

		@Override
		public Deque<CalculatorInput> removeLast()
		{
			return deque.removeLast();
		}

		@Override
		public Deque<CalculatorInput> pollFirst()
		{
			return deque.pollFirst();
		}

		@Override
		public Deque<CalculatorInput> pollLast()
		{
			return deque.pollLast();
		}

		@Override
		public Deque<CalculatorInput> getFirst()
		{
			return deque.getFirst();
		}

		@Override
		public Deque<CalculatorInput> getLast()
		{
			return deque.getLast();
		}

		@Override
		public Deque<CalculatorInput> peekFirst()
		{
			return deque.peekFirst();
		}

		@Override
		public Deque<CalculatorInput> peekLast()
		{
			return deque.peekLast();
		}

		@Override
		public boolean removeFirstOccurrence(Object o)
		{
			return deque.removeFirstOccurrence(o);
		}

		@Override
		public boolean removeLastOccurrence(Object o)
		{
			return deque.removeLastOccurrence(o);
		}

		@Override
		public boolean add(Deque<CalculatorInput> e)
		{
			return deque.add(e);
		}

		@Override
		public boolean offer(Deque<CalculatorInput> e)
		{
			return deque.offer(e);
		}

		@Override
		public Deque<CalculatorInput> remove()
		{
			return deque.remove();
		}

		@Override
		public Deque<CalculatorInput> poll()
		{
			return deque.poll();
		}

		@Override
		public Deque<CalculatorInput> element()
		{
			return deque.element();
		}

		@Override
		public Deque<CalculatorInput> peek()
		{
			return deque.peek();
		}

		@Override
		public void push(Deque<CalculatorInput> e)
		{
			deque.push(e);
		}

		@Override
		public Deque<CalculatorInput> pop()
		{
			return deque.pop();
		}

		@Override
		public boolean remove(Object o)
		{
			return deque.remove(o);
		}

		@Override
		public boolean contains(Object o)
		{
			return deque.contains(o);
		}

		@Override
		public int size()
		{
			return deque.size();
		}

		@Override
		public Iterator<Deque<CalculatorInput>> iterator()
		{
			return deque.iterator();
		}

		@Override
		public Iterator<Deque<CalculatorInput>> descendingIterator()
		{
			return deque.descendingIterator();
		}

		@Override
		public boolean isEmpty()
		{
			return deque.isEmpty();
		}

		@Override
		public Object[] toArray()
		{
			return deque.toArray();
		}

		@Override
		public <T> T[] toArray(T[] a)
		{
			return deque.toArray(a);
		}

		@Override
		public boolean containsAll(Collection<?> c)
		{
			return deque.containsAll(c);
		}

		@Override
		public boolean addAll(Collection<? extends Deque<CalculatorInput>> c)
		{
			return deque.addAll(c);
		}

		@Override
		public boolean removeAll(Collection<?> c)
		{
			return deque.removeAll(c);
		}

		@Override
		public boolean removeIf(Predicate<? super Deque<CalculatorInput>> filter)
		{
			return deque.removeIf(filter);
		}

		@Override
		public boolean retainAll(Collection<?> c)
		{
			return deque.retainAll(c);
		}

		@Override
		public void clear()
		{
			deque.clear();
		}

		@Override
		public Spliterator<Deque<CalculatorInput>> spliterator()
		{
			return deque.spliterator();
		}

		@Override
		public Stream<Deque<CalculatorInput>> stream()
		{
			return deque.stream();
		}

		@Override
		public Stream<Deque<CalculatorInput>> parallelStream()
		{
			return deque.parallelStream();
		}

		@Override
		public void forEach(Consumer<? super Deque<CalculatorInput>> action)
		{
			deque.forEach(action);
		}

		@Override
		public int hashCode()
		{
			return deque.hashCode();
		}

		@Override
		public boolean equals(Object obj)
		{
			return deque.equals(obj);
		}

		@Override
		public String toString()
		{
			return deque.toString();
		}
		
		
	}
	
	class WrapperForInput implements Deque<CalculatorInput>
	{
	    private final CalculatorInput ci;
	    
	    public WrapperForInput(CalculatorInput ci)
	    {
	        this.ci = ci;
	    }

		@Override
		public void addFirst(CalculatorInput e)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public void addLast(CalculatorInput e)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean offerFirst(CalculatorInput e)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean offerLast(CalculatorInput e)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput removeFirst()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput removeLast()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput pollFirst()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput pollLast()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput getFirst()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput getLast()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput peekFirst()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput peekLast()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean removeFirstOccurrence(Object o)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean removeLastOccurrence(Object o)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean add(CalculatorInput e)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean offer(CalculatorInput e)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput remove()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput poll()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput element()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput peek()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public void push(CalculatorInput e)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public CalculatorInput pop()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean remove(Object o)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean contains(Object o)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public int size()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public Iterator<CalculatorInput> iterator()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public Iterator<CalculatorInput> descendingIterator()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean isEmpty()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public Object[] toArray()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public <T> T[] toArray(T[] a)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean containsAll(Collection<?> c)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean addAll(Collection<? extends CalculatorInput> c)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean removeAll(Collection<?> c)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public boolean retainAll(Collection<?> c)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public void clear()
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public void forEach(Consumer<? super CalculatorInput> action)
		{
			Deque.super.forEach(action); //To change body of generated methods, choose Tools | Templates.
		}
	}
	
	enum EntryState
	{
		OPD, AUTO, USER;
	}
	
	private Calculator()
	{
		this(ICalcDisplay.NO_DISPLAY);
	}

	private Calculator(ICalcDisplay<? super E> display)
	{
		this(display, CalcReadymade.PRIORITIES);
	}

	private Calculator(ICalcDisplay<? super E> display, CalcPriority priorities)
	{
		this(display, priorities, ICalcAlg.STACK_BASED);
	}

	private Calculator(ICalcDisplay<? super E> display, CalcPriority priorities, ICalcAlg alg)
	{
		this.display = display;
		this.priorities = priorities;
		this.alg = alg;
	}

	public ICalcDisplay<? super E> getDisplay()
	{
		return display;
	}

	/**
	 * Automatically updates {@code display}.
	 *
	 * @param display
	 */
	public void setDisplay(ICalcDisplay<? super E> display)
	{
		this.display = display;
		this.display.update(iterator());
	}

	public ICalcAlg getAlg()
	{
		return alg;
	}

	public void setAlg(ICalcAlg alg)
	{
		this.alg = alg;
	}

	public CalcPriority getPriorities()
	{
		return priorities;
	}

	public void setPriorities(CalcPriority priorities)
	{
		this.priorities = priorities;
	}

	@Override
	public boolean canInput(E e)
	{
		return inputHelper(e, false);
	}

	protected boolean needOpdBefore(CalcMathData mdata)
	{
		throw new UnsupportedOperationException();
		//return mdata instanceof CalcInfixOpr && !mdata.equals(PAROPEN) || mdata.equals(NEXTARG) || mdata instanceof PostfixUnaryOpr;
	}

	protected boolean needOpdAfter(CalcMathData mdata)
	{
		throw new UnsupportedOperationException();
//		return mdata instanceof CalcArithmetic && !( mdata.equals(PARCLOSE) || mdata instanceof PostfixUnaryOpr );
	}

	/**
	 *
	 * @param previousCi nullable
	 * @param nextE
	 * @return
	 */
	protected CalculatorInput makeInput(CalculatorInput previousCi, E nextE)
	{
		CalcMathData next = nextE.getCalcData();
		CalcOpr nextOpr = null;
		try
		{
			nextOpr = (CalcOpr)next;
		}
		catch (ClassCastException exc1)
		{
		}

		if (previousCi == null)
		{
			try
			{
				CalcDigit nextDigit = (CalcDigit)next;
				if (nextDigit.isPoint())
				{
					return new CalculatorInput(nextE, CalculatorInputInfo.DECIMAL);
				}
				else if (nextDigit.isZero())
				{
					return new CalculatorInput(nextE, CalculatorInputInfo.INITIAL0);
				}

				return new CalculatorInput(nextE, CalculatorInputInfo.INTEGER);
			}
			catch (ClassCastException exc1)
			{
			}

			if (next instanceof CalcValue
					|| next instanceof CalcMathFunc
					|| CalcPriority.isPrefixUnaryOpr(nextOpr, priorities))
			{
				return new CalculatorInput(nextE, CalculatorInputInfo.INITIAL0);
			}

			return new CalculatorInput(nextE, CalculatorInputInfo.WRONG);
		}

		CalcMathData previous = previousCi.mdata.getCalcData();
		CalcOpr previousOpr = null;
		try
		{
			previousOpr = (CalcOpr)previous;
		}
		catch (ClassCastException exc1)
		{
		}

		try
		{
			CalcDigit nextDigit = (CalcDigit)next;

			if (previous instanceof CalcValue
					|| previous instanceof CalcLastArgSign
					|| CalcPriority.isPostfixUnaryOpr(previousOpr, priorities))
			{
				return new CalculatorInput(nextE, CalculatorInputInfo.WRONG);
			}

			if (nextDigit.isPoint())
			{
				if (previousCi.info.isDecimal())
				{
					return new CalculatorInput(nextE, CalculatorInputInfo.WRONG);
				}

				return new CalculatorInput(nextE, CalculatorInputInfo.DECIMAL);
			}
			if (!previousCi.info.isInitial0())
			{
				return new CalculatorInput(nextE, previousCi.info);
			}
			if (nextDigit.isZero())
			{
				return new CalculatorInput(nextE, CalculatorInputInfo.INITIAL0);
			}

			return new CalculatorInput(nextE, CalculatorInputInfo.INTEGER);
		}
		catch (ClassCastException exc1)
		{
		}

		if (next instanceof CalcMathSign)
		{
			if (next instanceof CalcNextArgSign)
			{
				if (previous instanceof CalcDigit
						|| previous instanceof CalcValue
						|| previous instanceof CalcLastArgSign
						|| CalcPriority.isPostfixUnaryOpr(previousOpr, priorities))
				{
					if (previousCi.calcMathFunc > 0)
					{
						return new CalculatorInput(nextE, CalculatorInputInfo.INITIAL0); // TODO
					}
				}

				return new CalculatorInput(nextE, CalculatorInputInfo.WRONG);
			}
			else if (next instanceof CalcLastArgSign)
			{
				if (previous instanceof CalcDigit
						|| previous instanceof CalcValue
						|| previous instanceof CalcLastArgSign
						|| CalcPriority.isPostfixUnaryOpr(previousOpr, priorities))
				{
					if (previousCi.calcMathFunc > 0)
					{
						return new CalculatorInput(nextE, CalculatorInputInfo.INITIAL0); // TODO
					}
				}

				return new CalculatorInput(nextE, CalculatorInputInfo.WRONG);
			}
			else if (next instanceof CalcMathFunc
					|| CalcPriority.isPrefixUnaryOpr(nextOpr, priorities))
			{
				if (previous instanceof CalcDigit)
				{
					if (!previousCi.info.isInitial0())
					{
						return new CalculatorInput(nextE, CalculatorInputInfo.WRONG);
					}
				}
				else if (previous instanceof CalcValue
						|| previous instanceof CalcLastArgSign
						|| CalcPriority.isPostfixUnaryOpr(previousOpr, priorities))
				{
					return new CalculatorInput(nextE, CalculatorInputInfo.WRONG);
				}
			}

			return new CalculatorInput(nextE, CalculatorInputInfo.INITIAL0);
		}
		else if (next instanceof CalcConstant)
		{
			if (previous instanceof CalcDigit)
			{
				if (!previousCi.info.isInitial0())
				{
					return new CalculatorInput(nextE, CalculatorInputInfo.WRONG);
				}
			}
			else if (previous instanceof CalcValue
						|| previous instanceof CalcLastArgSign
						|| CalcPriority.isPostfixUnaryOpr(previousOpr, priorities))
			{
				return new CalculatorInput(nextE, CalculatorInputInfo.WRONG);
			}

			return new CalculatorInput(nextE, CalculatorInputInfo.INITIAL0);
		}
		else if (next instanceof CalcAnswer)
		{
			return new CalculatorInput(nextE, CalculatorInputInfo.INITIAL0);
		}

		throw new UnsupportedOperationException();
	}

	@Override
	public boolean input(E e)
	{
		return inputHelper(e, true);
	}

	private boolean inputHelper(E e, boolean tryingTo)
	{
		CountingIterator<CalculatorInput> it = new CountingIteratorImpl<>(inputDeque.descendingIterator());
		CalculatorInput ci = screenInput(e, it);

		if (null == ci)
		{
			return false;
		}

		if (tryingTo)
		{
			int count = it.count();
			for (int i3 = 0; i3 < count - 1; ++i3)
			{
				delete();
			}
			inputDeque.add(ci);
			display.update(iterator());
		}

		return true;
	}

	/**
	 *
	 * @param e
	 *
	 * @param it The last <i>{@link CountingIterator#count()} - 1</i> elements in {@link #inputDeque}
	 * will be deleted at {@link #inputHelper(ICalcMathData, boolean)}
	 *
	 * @return
	 */
	protected CalculatorInput screenInput(E e, CountingIterator<CalculatorInput> it)
	{
		CalculatorInput ci = makeInput(it.next(), e);

		if (!ci.info.isWrong())
		{
			return ci;
		}

		return screenInput(ci, it);
	}

	/**
	 *
	 * @param ci
	 * @param it
	 * @return {@code null} if you don't want to input.
	 * @see #makeInput(calculator.ICalcMathData)
	 */
	protected abstract CalculatorInput screenInput(CalculatorInput ci, CountingIterator<CalculatorInput> it);

	@Override
	public Double answer()
	{
		Double result = null;
		try
		{
			result = ( (CalcAnswer)inputDeque.getLast().mdata.getCalcData() ).getValue();
		}
		catch (ClassCastException exc1)
		{
			try
			{
				result = alg.calculate(iteratorOfExpression(), priorities);
				input(makeAnswer(new CalcAnswer(result)));
			}
			catch (CalcWrongExpressionException exc3)
			{
				calcExceptionOccured = true;
				whenCalcExcOccured(exc3);
			}
		}

		return result;
	}

	protected abstract void whenCalcExcOccured(CalcException calcException);

	protected abstract E makeAnswer(CalcAnswer calcAnswer);

	@Override
	public void clear()
	{
		inputDeque.clear();
		calcExceptionOccured = false;
		display.update(iterator());
	}

	@Override
	public void clearEntry()
	{
		try
		{
			if (inputDeque.getLast().mdata instanceof CalcDigit)
			{
				do
				{
					delete();
				} while (inputDeque.getLast().mdata instanceof CalcDigit
						&& ( !inputDeque.getLast().info.isInitial0() || inputDeque.size() > 1 ));
			}
			else
			{
				delete();
			}
		}
		catch (NoSuchElementException exc1)
		{
			clear();
		}
	}

	@Override
	public void clearExpression()
	{

	}

	@Override
	public void delete()
	{
		deleteHelper();

		if (inputDeque.isEmpty())
		{
			clear();
		}
	}

	private void deleteHelper()
	{
		if (!calcExceptionOccured)
		{
			inputDeque.pollLast();
		}

		calcExceptionOccured = false;
		display.update(iterator());
	}

	@Override
	public Iterator<E> iterator()
	{
		return new CalcMathInputItAdapter(inputDeque.iterator());
	}

	@Override
	public Iterator<E> iteratorOfExpression()
	{
		return new CalcMathInputItAdapter(inputDeque.stream().skip(inputDeque.getLast().ansPos).iterator());
	}

	protected class CalculatorInput
	{
		private E mdata;
		private int ansPos;
		private int calcMathFunc;
		private CalculatorInputInfo info;

		CalculatorInput(E mdata)
		{
			this(mdata, inputDeque.getLast().info);
		}

		CalculatorInput(E mdata, CalculatorInputInfo calcDigitInfo)
		{
			this.mdata = mdata;
			this.info = calcDigitInfo;

			if (mdata instanceof CalcMathFunc)
			{
				calcMathFunc = 1;
			}
			else if (mdata instanceof CalcLastArgSign)
			{
				calcMathFunc = -1;
			}

			try
			{
				if (mdata instanceof CalcAnswer)
				{
					ansPos = inputDeque.size();
				}
				else
				{
					CalculatorInput last = inputDeque.peekLast();
					ansPos = last.ansPos;
					calcMathFunc += last.calcMathFunc;
				}
			}
			catch (NullPointerException exc1)
			{
				// There isn't CalcAnswer, but this value is used to skip no elements
				// in inputDeque.stream().skip(inputDeque.getLast().ansPos)
				ansPos = 0;
			}

			if (calcMathFunc < 0)
			{
				//throw new RuntimeException("parclseDemand < 0");
			}
		}

		public int getParclseDemand()
		{
			return calcMathFunc;
		}
	}

	private class CalcMathInputItAdapter implements Iterator<E>
	{
		private final Iterator<CalculatorInput> it;

		public CalcMathInputItAdapter(Iterator<CalculatorInput> it)
		{
			this.it = it;
		}

		@Override
		public boolean hasNext()
		{
			return it.hasNext();
		}

		@Override
		public E next()
		{
			return it.next().mdata;
		}
	}

	protected enum CalculatorInputInfo
	{
		WRONG, INITIAL0, INTEGER, DECIMAL,;

		public boolean isInitial0()
		{
			return this.equals(INITIAL0);
		}

		public boolean isDecimal()
		{
			return this.equals(DECIMAL);
		}

		public boolean isWrong()
		{
			return this.equals(WRONG);
		}
	}
	
	static class History
	{
		Deque<ICalcMathData> deque;
		CalcAnswer answer;

		public History(Deque<ICalcMathData> deque, CalcAnswer answer)
		{
			this.deque = deque;
			this.answer = answer;
		}
	}

	public static class InputException extends RuntimeException
	{
		private static final long serialVersionUID = 1L;
	}

	public static abstract class Factory<T extends ICalculator<?>> extends ClearableFactory<T>
	{
		@Override
		public T produce()
		{
			return super.produce();
		}

		protected abstract static class UninitializedCalculator<E extends ICalcMathData> extends Calculator<E>
		{
			public UninitializedCalculator()
			{
				super();
			}

			public UninitializedCalculator(ICalcDisplay<? super E> display)
			{
				super(display);
			}

			public UninitializedCalculator(ICalcDisplay<? super E> display, CalcPriority priorities)
			{
				super(display, priorities);
			}

			public UninitializedCalculator(ICalcDisplay<? super E> display, CalcPriority priorities, ICalcAlg alg)
			{
				super(display, priorities, alg);
			}
		}
	}
}
