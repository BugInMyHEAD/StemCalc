package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcArgumentException extends CalcWrongExpressionException
{
	public CalcArgumentException()
	{
		super();
	}
	
	public CalcArgumentException(String message)
	{
		super(message);
	}
}
