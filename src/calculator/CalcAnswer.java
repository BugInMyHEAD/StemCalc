
package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcAnswer extends CalcValue
{
	private final Double value;
	
	public CalcAnswer(Double value)
	{
		this.value = value;
	}
	
	@Override
	public Double getValue()
	{
		return value;
	}
	
	@Override
	public String toString()
	{
		return value.toString();
	}
}
