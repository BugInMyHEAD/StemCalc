/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

/**
 *
 * @author BugInMyHEAD
 */
public interface ICalcAlg
{
	ICalcAlg STACK_BASED = new ICalcAlg()
	{
		private final Deque<Double> figureValueDeque = new ArrayDeque<>();
		private final Deque<Integer> mathFuncArgcDeque = new ArrayDeque<>();

		@Override
		public Double calculate(Iterator<? extends ICalcMathData> it, CalcPriority priorities)
				throws CalcWrongExpressionException
		{
			Deque<CalcMathData> postfixNotated = new ArrayDeque<>();
			toPostfix(it, postfixNotated, priorities);

			figureValueDeque.clear();
			for (CalcMathData data : postfixNotated)
			{
				try
				{
					figureValueDeque.push(( (CalcValue)data ).getValue());

					continue;
				}
				catch (ClassCastException exc3)
				{
				}

				try
				{
					CalcArithmetic adata = (CalcArithmetic)data;
					ArrayList<Double> args;

					do
					{
						if (adata instanceof CalcMathFunc)
						{
							args = new ArrayList<>(mathFuncArgcDeque.pop());

							break;
						}

						try
						{
							args = new ArrayList<>(( (CalcOpr)adata ).argc);

							break;
						}
						catch (ClassCastException exc7)
						{
						}

						throw new UnsupportedOperationException("Unknown 'CalcMathSign' type");
					} while (false);

					for (int i5 = args.size() - 1; 0 <= i5; i5--)
					{
						args.set(i5, figureValueDeque.pop());
					}
					figureValueDeque.push(adata.getResult(args));

					continue;
				}
				catch (ClassCastException exc3)
				{
				}

				throw new UnsupportedOperationException("Unknown 'CalcMathData' type");
			}

			if (figureValueDeque.size() != 1)
			{
				throw new CalcWrongExpressionException(figureValueDeque.size() + "");
			}

			return figureValueDeque.pop();
		}

		/**
		 * Also converts {@link CalcDigit} to {@link CalcConstant}.
		 *
		 * @param postfix in which the postfix notation will be written.
		 */
		private void toPostfix(Iterator<? extends ICalcMathData> infix, Collection<CalcMathData> postfix, CalcPriority priorities)
				throws CalcWrongExpressionException
		{
			mathFuncArgcDeque.clear();
			Deque<CalcMathSign> arithmeticDeque = new ArrayDeque<>();
			StringBuilder sbForDigits = new StringBuilder(Double.MAX_EXPONENT + 1);

			while (infix.hasNext())
			{
				CalcMathData data = infix.next().getCalcData();

				try
				{
					sbForDigits.append((CalcDigit)data);

					continue;
				}
				catch (ClassCastException exc3)
				{
				}

				try
				{
					CalcMathSign arithmeticData = (CalcMathSign)data;

					if (sbForDigits.length() > 0)
					{
						postfix.add(new CalcConstant(Double.valueOf(sbForDigits.toString())));
						sbForDigits.setLength(0);
					}

					if (arithmeticData instanceof CalcLastArgSign)
					{
						CalcMathSign poppedData;
						do
						{
							poppedData = arithmeticDeque.pop();
							postfix.add(poppedData);
						} while (!( poppedData instanceof CalcMathFunc ));

						continue; // to deal with the next CalcMathData
					}
					if (arithmeticData instanceof CalcNextArgSign)
					{
						mathFuncArgcDeque.push(mathFuncArgcDeque.pop() + 1);
						while (!( arithmeticDeque.peek() instanceof CalcMathFunc ))
						{
							postfix.add(arithmeticDeque.pop());
						}

						continue; // to deal with the next CalcMathData
					}

					if (arithmeticData instanceof CalcMathFunc)
					{
						mathFuncArgcDeque.push(1);
					}
					//else if (!( arithmeticData instanceof CalcUnaryOpr && arithmeticData.beforeOpd))
					{
						//while (!arithmeticDeque.isEmpty() && ( (CalcOpr)arithmeticDeque.peek() ).compareTo((CalcOpr)arithmeticData) <= 0)
						{
							postfix.add(arithmeticDeque.pop());
						}
					}

					arithmeticDeque.push(arithmeticData);
				}
				catch (ClassCastException exc3)
				{
					if (sbForDigits.length() > 0)
					{
						throw new CalcWrongExpressionException();
					}
					postfix.add((CalcConstant)data);
				}
			}

			if (sbForDigits.length() > 0)
			{
				postfix.add(new CalcConstant(Double.valueOf(sbForDigits.toString())));
			}

			while (!arithmeticDeque.isEmpty())
			{
				CalcMathSign arithmeticData = arithmeticDeque.pop();
				if (arithmeticData instanceof CalcMathFunc)
				{
					throw new CalcWrongExpressionException();
				}
				postfix.add(arithmeticData);
			}
		}
	};
	
	Double calculate(Iterator<? extends ICalcMathData> it, CalcPriority priorities) throws CalcWrongExpressionException;
}
