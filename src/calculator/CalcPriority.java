/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcPriority
{
	/**
	 *
	 *
	 * @param opr
	 * @param priority
	 * @return If 'opr' is {@code null}, {@code false}
	 * @see #isPostfixUnaryOpr(CalcOpr, CalcPriority)
	 */
	static boolean isPrefixUnaryOpr(CalcOpr opr, CalcPriority priority)
	{
		return CalcOpr.hasArgc(opr, 1) && priority.getCalcOprSet(opr).rightToLeft;
	}

	/**
	 *
	 *
	 * @param opr
	 * @param priority
	 * @return If 'opr' is {@code null}, {@code false}
	 * @see #isPrefixUnaryOpr(CalcOpr, CalcPriority)
	 */
	static boolean isPostfixUnaryOpr(CalcOpr opr, CalcPriority priority)
	{
		return CalcOpr.hasArgc(opr, 1) && !priority.getCalcOprSet(opr).rightToLeft;
	}

	private final Map<CalcOpr, PriorityElement> map = new HashMap<>();
	private final Set<PriorityElement> set;

	@SafeVarargs
	public CalcPriority(CalcOprSet... oprSet)
	{
		set = IntStream.iterate(0, (i) -> i + 1)
				.limit(oprSet.length)
				.parallel()
				.mapToObj((i) -> new PriorityElement(oprSet[i], i))
				.collect(Collectors.toCollection(() -> new TreeSet<>((a, b) -> a.priority - b.priority)));
		set.forEach(
				(pe) -> pe.oprSet.forEach(
						(opr) ->
				{
					if (null != map.putIfAbsent(opr, pe))
					{
						throw new IllegalArgumentException("There is a duplicate of 'CalcOpr's in another 'CalcOprSet's");
					}
				}));
	}

	public CalcOprSet getCalcOprSet(CalcOpr o)
	{
		return map.get(o).oprSet;
	}

	public int compare(CalcOpr a, CalcOpr b)
	{
		return map.get(a).priority - map.get(b).priority;
	}

	public int compareLR(CalcOpr left, CalcOpr right)
	{
		int result;

		result = compare(left, right);
		if (0 == result)
		{
			result = map.get(left).oprSet.rightToLeft ? 1 : -1;
		}
		else
		{
			result += Integer.signum(result);
		}

		return result;
	}

	private static class PriorityElement
	{
		private final CalcOprSet oprSet;
		private int priority;

		public PriorityElement(CalcOprSet oprSet, int priority)
		{
			this.oprSet = oprSet;
			this.priority = priority;
		}
	}
}
