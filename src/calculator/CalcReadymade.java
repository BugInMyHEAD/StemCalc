/*
 * The MIT License
 *
 * Copyright 2018 BugInMyHEAD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package calculator;

import java.util.ArrayList;
import java.util.function.IntPredicate;
import java.util.stream.DoubleStream;

/**
 *
 * @author BugInMyHEAD
 */
public final class CalcReadymade
{
	public static final CalcLastArgSign PARCLOSE = new CalcLastArgSign(")");
	public static final CalcNextArgSign NEXTARG = new CalcNextArgSign(",");

	public static final CalcDigit POINT = CalcDigit.makePoint(".");
	private static final CalcDigit[] DIGITS =
	{
		new CalcDigit("0", 0),
		new CalcDigit("1", 1),
		new CalcDigit("2", 2),
		new CalcDigit("3", 3),
		new CalcDigit("4", 4),
		new CalcDigit("5", 5),
		new CalcDigit("6", 6),
		new CalcDigit("7", 7),
		new CalcDigit("8", 8),
		new CalcDigit("9", 9),
		new CalcDigit("A", 0x0a),
		new CalcDigit("B", 0x0b),
		new CalcDigit("C", 0x0c),
		new CalcDigit("D", 0x0d),
		new CalcDigit("E", 0x0e),
		new CalcDigit("F", 0x0f),
	};
	public static final int MAX_RADIX = DIGITS.length;

	public static final CalcOpr UADD = new CalcOpr("+", 1, (opds) -> +opds.get(0));
	public static final CalcOpr USUB = new CalcOpr("-", 1, (opds) -> -opds.get(0));

	public static final CalcOpr ADD = new CalcOpr("+", 2, (opds) -> opds.get(0) + opds.get(1));
	public static final CalcOpr SUB = new CalcOpr("-", 2, (opds) -> opds.get(0) - opds.get(1));
	public static final CalcOpr MUL = new CalcOpr("*", 2, (opds) -> opds.get(0) * opds.get(1));
	public static final CalcOpr DIV = new CalcOpr("/", 2, (opds) -> opds.get(0) / opds.get(1));
	public static final CalcOpr MOD = new CalcOpr("%", 2, (opds) -> opds.get(0) % opds.get(1));
	public static final CalcOpr POW = new CalcOpr("^", 2, (opds) -> Math.pow(opds.get(0), opds.get(1)));
	// TODO: piled arrow opr

	public static final CalcConstant E = new CalcConstant(Math.E, "e");
	public static final CalcConstant PI = new CalcConstant(Math.PI, "π");

	public static final CalcVariable RAND = new CalcVariable(Math::random, "rand");

	public static final CalcMathFunc PAROPEN = new MathFuncOpeningPar("", (argc) -> 1 == argc, (opds) -> opds.get(0));

	public static final CalcMathFunc SUM = new MathFuncOpeningPar(
			"sum",
			(argc) -> argc > 0,
			(opds) -> toParallelDoubleStream(opds).sum());
	public static final CalcMathFunc AVG = new MathFuncOpeningPar(
			"avg",
			(argc) -> argc > 0,
			(opds) -> toParallelDoubleStream(opds).average().getAsDouble());

	protected static final ICalcOperation FACTORIAL = opds
			-> DoubleStream.concat(DoubleStream.of(1), DoubleStream.iterate(1, (n) -> n + 1))
					.limit(Math.max(0, Math.round(Math.floor(opds.get(0)))))
					.parallel()
					.reduce((a, b) -> a * b)
					.getAsDouble();
	public static final CalcOpr FACTOPR = new CalcOpr("!", 1, FACTORIAL);
	public static final CalcMathFunc FACTFUNC = new MathFuncOpeningPar("fact", (argc) -> 1 == argc, FACTORIAL);

	protected static final ICalcOperation SQUARE_ROOT = (opds) -> Math.sqrt(opds.get(0));
	public static final CalcOpr SQRTOPR = new CalcOpr("√", 1, SQUARE_ROOT);
	public static final CalcMathFunc SQRTFUNC = new MathFuncOpeningPar("sqrt", (argc) -> 1 == argc, SQUARE_ROOT);

	private static final CalcOprSet[] SET =
	{
		new CalcOprSet(false, CalcReadymade.FACTOPR),
		new CalcOprSet(true, CalcReadymade.UADD, CalcReadymade.USUB, CalcReadymade.SQRTOPR),
		new CalcOprSet(false, CalcReadymade.POW),
		new CalcOprSet(false, CalcReadymade.ADD, CalcReadymade.SUB, CalcReadymade.MUL, CalcReadymade.DIV, CalcReadymade.MOD),
	};
	public static final CalcPriority PRIORITIES = new CalcPriority(SET);

	public static CalcDigit digit(int d)
	{
		return DIGITS[d];
	}

	protected static DoubleStream toParallelDoubleStream(ArrayList<Double> opds)
	{
		return opds.parallelStream().mapToDouble((opd) -> opd);
	}

	protected static class MathFuncOpeningPar extends CalcMathFunc
	{
		public MathFuncOpeningPar(String string, IntPredicate argcChecker, ICalcOperation operation)
		{
			super(string, argcChecker, operation);
		}

		@Override
		public String toString()
		{
			return super.toString() + "(";
		}
	}
}
