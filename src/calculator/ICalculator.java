package calculator;

import library.Clearable;
import java.util.*;

/**
 *
 * @author BugInMyHEAD
 * @param <E>
 */
public interface ICalculator<E extends ICalcMathData> extends Clearable, Iterable<E>
{
	/**
	 * It's an implementaion of 'answer' function of a virtual Calculator.
	 * @return The result of calculation
	 */
	Double answer();
		
	/**
	 * 
	 * @return {@link Iterator} of {@link CalcMathData}s stored in the implementation in order.
	 */
	@Override
	Iterator<E> iterator();

	/**
	 * 
	 * @return {@link Iterator} of {@link CalcMathData}s of the last expression in order.
	 */
	Iterator<E> iteratorOfExpression();
	
	/**
	 * 
	 * @param t
	 * @return Whether {@code mdata} is inputtable. It depends on the state and policies of the implementaion.
	 * @see input(CalcMathData)
	 */
	boolean canInput(E t);

	/**
	 * 
	 * @param t
	 * @return Whether {@code mdata} has been input. It depends on the state and policies of the implementaion.
	 * @see canInput(CalcMathData)
	 */
	boolean input(E t);
	
	@Override
	void clear();

	void clearEntry();
	
	void clearExpression();

	void delete();
}
