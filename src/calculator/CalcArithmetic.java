package calculator;

import java.util.ArrayList;

/**
 *
 * @author BugInMyHEAD
 */
public abstract class CalcArithmetic extends CalcMathSign
{
	private final ICalcOperation operation;
	
	protected CalcArithmetic(String text, ICalcOperation operation)
	{
		super(text);
		this.operation = operation;
	}
	
	public Double getResult(ArrayList<Double> opds)
	{
		if (!argcCheck(opds.size()))
		{
			throw new IllegalArgumentException("argc != opds.length");
		}
		
		return operation.applyAsDouble(opds);
	}
	
	public abstract boolean argcCheck(int argc);
}
