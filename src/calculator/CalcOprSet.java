/*
 * The MIT License
 *
 * Copyright 2018 BugInMyHEAD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package calculator;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Unmodifiable set.
 * @author BugInMyHEAD
 */
public class CalcOprSet implements Set<CalcOpr>
{
	public final boolean rightToLeft;
	private final Set<CalcOpr> set;

	@SafeVarargs
	public CalcOprSet(boolean rightToLeft, CalcOpr... oprs)
	{
		this(rightToLeft, Arrays.asList(oprs));
	}

	public CalcOprSet(boolean rightToLeft, Collection<? extends CalcOpr> c)
	{
		set = Collections.unmodifiableSet(c.parallelStream().map(CalcOpr::requireInfixNotatable).collect(Collectors.toSet()));
		this.rightToLeft = rightToLeft;
	}

	@Override
	public int size()
	{
		return set.size();
	}

	@Override
	public boolean isEmpty()
	{
		return set.isEmpty();
	}

	@Override
	public boolean contains(Object o)
	{
		return set.contains(o);
	}

	@Override
	public Iterator<CalcOpr> iterator()
	{
		return set.iterator();
	}

	@Override
	public Object[] toArray()
	{
		return set.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a)
	{
		return set.toArray(a);
	}

	@Override
	public boolean add(CalcOpr e)
	{
		return set.add(e);
	}

	@Override
	public boolean remove(Object o)
	{
		return set.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		return set.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends CalcOpr> c)
	{
		return set.addAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		return set.retainAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		return set.removeAll(c);
	}

	@Override
	public void clear()
	{
		set.clear();
	}

	@Override
	public int hashCode()
	{
		return set.hashCode() * ( rightToLeft ? -1 : 1 );
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (obj == null)
		{
			return false;
		}

		if (this.getClass() != obj.getClass())
		{
			return false;
		}

		CalcOprSet other = (CalcOprSet)obj;
		return this.set.equals(other.set) && this.rightToLeft == other.rightToLeft;
	}

	@Override
	public Spliterator<CalcOpr> spliterator()
	{
		return set.spliterator();
	}

	@Override
	public boolean removeIf(Predicate<? super CalcOpr> filter)
	{
		return set.removeIf(filter);
	}

	@Override
	public String toString()
	{
		return ( rightToLeft ? "rtl" : "ltr" ) + " " + set.toString();
	}

	@Override
	public Stream<CalcOpr> stream()
	{
		return set.stream();
	}

	@Override
	public Stream<CalcOpr> parallelStream()
	{
		return set.parallelStream();
	}

	@Override
	public void forEach(Consumer<? super CalcOpr> action)
	{
		set.forEach(action);
	}
}
