package calculator;

import java.util.function.IntPredicate;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcMathFunc extends CalcArithmetic
{
	private final IntPredicate argcChecker;
	
	/**
	 * Constructs {@link CalcMathFunc} that can have {@code minArgc} to {@code maxArgc} arguments.
	 * @param text
	 * @param argcChecker
	 * @param operation
	 */
	public CalcMathFunc(String text, IntPredicate argcChecker, ICalcOperation operation)
	{
		super(text, operation);
		this.argcChecker = argcChecker;
	}

	@Override
	public boolean argcCheck(int argc)
	{
		return argcChecker.test(argc);
	}
}