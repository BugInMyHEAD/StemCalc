/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcOpr extends CalcArithmetic
{
	public static CalcOpr requireInfixNotatable(CalcOpr o)
	{
		if (o.isInfixNotatable())
		{
			return o;
		}

		throw new IllegalArgumentException(
				"'CalcOpr' in 'CalcOprSet' must be infix notatable (see CalcOpr#isInfixNotatable())");
	}

	/**
	 *
	 *
	 * @param opr
	 * @return If 'opr' is {@code null}, {@code false}
	 * @see CalcPriorities#isPostfixUnaryOpr(CalcOpr, CalcPriorities)
	 * @see CalcPriorities#isPrefixUnaryOpr(CalcOpr, CalcPriorities)
	 */
	static boolean hasArgc(CalcOpr opr, int argc)
	{
		return opr != null && opr.argcCheck(argc);
	}

	/**
	 * Required number of operands of {@code this}
	 */
	public final int argc;

	public CalcOpr(String text, int argc, ICalcOperation operation)
	{
		super(text, operation);
		this.argc = argc;
	}

	@Override
	public boolean argcCheck(int argc)
	{
		return argc == this.argc;
	}

	public boolean isInfixNotatable()
	{
		return argc == 1 || argc == 2;
	}
}
