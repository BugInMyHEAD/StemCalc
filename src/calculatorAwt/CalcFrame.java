package calculatorAwt;

import library.CountingIterator;
import calculator.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcFrame extends Frame
{
	private static final long serialVersionUID = 1L;

	static final GridBagLayout BUTTON_LAYOUT = new GridBagLayout();
	static final GridLayout DISPLAY_LAYOUT = new GridLayout(2, 0);

	public static void main(String[] args)
	{
		CalcFrame calcFrameA = new CalcFrame("Calculator");
		calcFrameA.setVisible(true);
	}

	Panel buttonPanel = new Panel(BUTTON_LAYOUT);
	Panel displayPanel = new Panel(DISPLAY_LAYOUT);

	TextField expression = new TextField();
	TextField answer = new TextField();
	ICalculator<ICalcMathData> calculator = new Calculator.Factory<ICalculator<ICalcMathData>>()
	{
		@Override
		protected ICalculator<ICalcMathData> design()
		{
			return new UninitializedCalculator<ICalcMathData>((it) ->
			{
				
			})
			{
				@Override
				public void clear()
				{
					//super.clear(); //To change body of generated methods, choose Tools | Templates.
				}

				@Override
				protected ICalcMathData makeAnswer(CalcAnswer calcAnswer)
				{
					return calcAnswer;
				}

				@Override
				protected Calculator.CalculatorInput screenInput(Calculator.CalculatorInput ci, CountingIterator it)
				{
					throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
				}

				@Override
				protected void whenCalcExcOccured(CalcException calcException)
				{
					throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
				}
			};
		}
	}.produce();



	
	{
		setSize(500, 700);
		add(displayPanel, BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.CENTER);
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				System.exit(0);
			}
		});

		displayPanel.add(expression);
		displayPanel.add(answer);
		Font displayFont = new Font(Font.MONOSPACED, Font.PLAIN, 30);
		expression.setEditable(false);
		expression.setFont(displayFont);
		answer.setEditable(false);
		answer.setFont(displayFont);

		CalcButton calcButton = new CalcButton(calculator);
		calcButton.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 20));
		GridBagConstraints gbcA = new GridBagConstraints();
		gbcA.fill = GridBagConstraints.BOTH;
		gbcA.weightx = 1;
		gbcA.weighty = 1;
		gbcA.insets = new Insets(6, 6, 6, 6);
		gbcA.gridx = 0;
		buttonPanel.add(calcButton.del, gbcA);
		gbcA.gridx = 1;
		buttonPanel.add(calcButton.ce, gbcA);
		gbcA.gridx = 2;
		buttonPanel.add(calcButton.clr, gbcA);
		{
			int i1 = 9;
			for (gbcA.gridy = 1; gbcA.gridy < 4; gbcA.gridy++)
			{
				for (gbcA.gridx = 2; gbcA.gridx >= 0; gbcA.gridx--)
				{
					buttonPanel.add(calcButton.digit[i1--], gbcA);
				}
			}
		}
		gbcA.gridx = 0;
		gbcA.gridwidth = 2;
		buttonPanel.add(calcButton.digit[0], gbcA);
		gbcA.gridwidth = 1;
		gbcA.gridx = 2;
		buttonPanel.add(calcButton.dot, gbcA);

		gbcA.gridx = 3;
		gbcA.gridy = 0;
		buttonPanel.add(calcButton.ans, gbcA);
		gbcA.gridy = 1;
		buttonPanel.add(calcButton.add, gbcA);
		gbcA.gridy = 2;
		buttonPanel.add(calcButton.sub, gbcA);
		gbcA.gridy = 3;
		buttonPanel.add(calcButton.mul, gbcA);
		gbcA.gridy = 4;
		buttonPanel.add(calcButton.div, gbcA);

		gbcA.gridx = 4;
		gbcA.gridy = 0;
		buttonPanel.add(calcButton.nextarg, gbcA);
		gbcA.gridy = 1;
		buttonPanel.add(calcButton.pi, gbcA);
		gbcA.gridy = 2;
		buttonPanel.add(calcButton.e, gbcA);
		gbcA.gridy = 3;
		buttonPanel.add(calcButton.paropen, gbcA);
		gbcA.gridy = 4;
		buttonPanel.add(calcButton.parclose, gbcA);

		gbcA.gridx = 5;
		gbcA.gridy = 0;
		buttonPanel.add(calcButton.factopr, gbcA);
		gbcA.gridy = 1;
		buttonPanel.add(calcButton.factfunc, gbcA);
		gbcA.gridy = 2;
		buttonPanel.add(calcButton.sum, gbcA);
		gbcA.gridy = 3;
		buttonPanel.add(calcButton.sqrtopr, gbcA);
		gbcA.gridy = 4;
		buttonPanel.add(calcButton.sqrtfunc, gbcA);
	}

	public CalcFrame()
	{
	}

	public CalcFrame(String title)
	{
		super(title);
	}
}
