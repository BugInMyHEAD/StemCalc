package calculatorAwt;

import calculator.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author BugInMyHEAD
 */
public class CalcButton
{
	private ICalculator<ICalcMathData> calculator;
	
	private final ArrayList<Button> buttonList = new ArrayList<>();
	
	Button[] digit;
	Button dot = makeButton(CalcReadymade.POINT.toString(), (ev) -> calculator.input(CalcReadymade.POINT));
	Button clr = makeButton("C", (ev) -> calculator.clear());
	Button ce = makeButton("CE", (ev) -> calculator.clearEntry());
	Button del = makeButton("DEL", (ev) -> calculator.delete());
	Button ans = makeButton("=", (ev) -> calculator.answer());
	
	Button add = makeButton(CalcReadymade.ADD.toString(), (ev) -> calculator.input(CalcReadymade.ADD));
	Button sub = makeButton(CalcReadymade.SUB.toString(), (ev) -> calculator.input(CalcReadymade.SUB));
	Button mul = makeButton(CalcReadymade.MUL.toString(), (ev) -> calculator.input(CalcReadymade.MUL));
	Button div = makeButton(CalcReadymade.DIV.toString(), (ev) -> calculator.input(CalcReadymade.DIV));
	Button mod = makeButton(CalcReadymade.MOD.toString(), (ev) -> calculator.input(CalcReadymade.MOD));
	Button factopr = makeButton(CalcReadymade.FACTOPR.toString(), (ev) -> calculator.input(CalcReadymade.FACTOPR));
	Button sqrtopr = makeButton(CalcReadymade.SQRTOPR.toString(), (ev) -> calculator.input(CalcReadymade.SQRTOPR));
	
	Button paropen = makeButton(CalcReadymade.PAROPEN.toString(), (ev) -> calculator.input(CalcReadymade.PAROPEN));
	Button parclose = makeButton(CalcReadymade.PARCLOSE.toString(), (ev) -> calculator.input(CalcReadymade.PARCLOSE));
	Button nextarg = makeButton(CalcReadymade.NEXTARG.toString(), (ev) -> calculator.input(CalcReadymade.NEXTARG));
	
	Button pi = makeButton(CalcReadymade.PI.toString(), (ev) -> calculator.input(CalcReadymade.PI));
	Button e = makeButton(CalcReadymade.E.toString(), (ev) -> calculator.input(CalcReadymade.E));
	
	Button sum = makeButton(CalcReadymade.SUM.toString(), (ev) -> calculator.input(CalcReadymade.SUM));
	Button factfunc = makeButton(CalcReadymade.FACTFUNC.toString(), (ev) -> calculator.input(CalcReadymade.FACTFUNC));
	Button sqrtfunc = makeButton(CalcReadymade.SQRTFUNC.toString(), (ev) -> calculator.input(CalcReadymade.SQRTFUNC));
	
	private Button makeButton(String label, ActionListener... actionListener)
	{
		Button button = new Button(label);
		buttonList.add(button);
		for(ActionListener alA1 : actionListener)
		{
			button.addActionListener(alA1);
		}
		button.setPreferredSize(new Dimension(0, 0));
		
		return button;
	}
	
	public CalcButton(ICalculator<ICalcMathData> calculator)
	{
		this.calculator = calculator;
		digit = new Button[CalcReadymade.MAX_RADIX];
		
		for(int i1 = 0;  i1 < digit.length;  i1++)
		{
			final int i2 = i1;
			digit[i2] = makeButton(CalcReadymade.digit(i2).toString(), (ev) -> calculator.input(CalcReadymade.digit(i2)));
		}
	}
	
	public void setFont(Font font)
	{
		buttonList.forEach((button) -> button.setFont(font));
	}
}
